package com.controller.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.bean.User;


class UserControllerTest {
	String baseUrl ="http://localhost:8282/userCurd";

	

	@Test
	void testGetAllUser() {
		RestTemplate restTemplate = new RestTemplate();
		String url=baseUrl+"/getAllUser";
		ResponseEntity<List<User>> listOfUser = restTemplate.exchange(url,HttpMethod.GET,null,new ParameterizedTypeReference<List<User>>() {});
		List<User>usersList= listOfUser.getBody(); 
		Assertions.assertTrue(usersList.stream().anyMatch(u->u.getUsername().equals("ramesh")));
	}


}
