package com.dao;




import org.springframework.data.jpa.repository.JpaRepository;


import com.bean.CompositeKey;
import com.bean.LikedBooks;

public interface LikedBooksDao extends JpaRepository<LikedBooks, CompositeKey>  {

}