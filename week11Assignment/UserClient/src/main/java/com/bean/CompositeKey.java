package com.bean;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class CompositeKey implements Serializable{
	private String emailId;
	private int bId;
	public CompositeKey() {
		super();
		// TODO Auto-generated constructor stub
	}
	public  CompositeKey(String emailId, int bookId) {
		super();
		this.emailId = emailId;
		this.bId = bId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public int getBId() {
		return bId;
	}
	public void setBId(int bId) {
		this.bId = bId;
	}
	@Override
	public String toString() {
		return "CompositeKey [emailId=" + emailId + ", bId=" + bId + "]";
	}

}
